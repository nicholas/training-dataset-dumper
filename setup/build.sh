# convience script to build the code after setting up a release

# determine appropriate directory for build/
SCRIPT_PATH=${BASH_SOURCE[0]:-${0}}
TOPDIR=${SCRIPT_PATH%/*}/../../
echo "=== cd to `realpath $TOPDIR` ==="
cd $TOPDIR

echo "=== recreate build dir ==="
rm -r build
mkdir build
cd build

echo "=== running cmake/make ==="
cmake ../training-dataset-dumper
make -j8

echo "=== run x*/setup.sh ==="
source x*/setup.sh
cd ..
