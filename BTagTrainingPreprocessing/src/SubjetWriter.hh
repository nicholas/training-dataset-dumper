#ifndef SUBJET_WRITER_HH
#define SUBJET_WRITER_HH


namespace H5 {
  class Group;
}
namespace xAOD {
  class Jet_v1;
  typedef Jet_v1 Jet;
  class EventInfo_v1;
  typedef EventInfo_v1 EventInfo;
}

class SubjetWriterConfig;
class IJetOutputWriter;

#include <string>
#include <vector>
#include <map>
#include <memory>

class SubjetWriter
{
public:
  SubjetWriter(
    H5::Group& output_file,
    const SubjetWriterConfig& jet);
  ~SubjetWriter();
  SubjetWriter(SubjetWriter&&);
  SubjetWriter(SubjetWriter&) = delete;
  SubjetWriter operator=(SubjetWriter&) = delete;
  void write_dummy();
  void write_with_parent(std::vector<const xAOD::Jet*> jets,
                         const xAOD::Jet* parent);
private:
  std::unique_ptr<IJetOutputWriter> m_hdf5_jet_writer;
};


#endif
