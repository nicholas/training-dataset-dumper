#include "SingleBTagAlg.h"
#include "SingleBTagElectronAlg.h"

#include "SingleBTagTools.hh"
#include "SingleBTagConfig.hh"
#include "processSingleBTagEvent.hh"
#include "ConfigFileTools.hh"

#include "H5Cpp.h"

#include "StoreGate/ReadHandle.h"

#include <nlohmann/json.hpp>
#include <filesystem>

#include "BTagElectronAugmenter.hh"
#include "ElectronPhotonSelectorTools/AsgElectronSelectorTool.h"
#include "ElectronPhotonSelectorTools/ElectronSelectorHelpers.h"

void check_rc(StatusCode code) {
  if (!code.isSuccess()) throw std::runtime_error("bad return code");
}

SingleBTagElectronAlg::SingleBTagElectronAlg(const std::string& name,
                                             ISvcLocator* pSvcLocator):
  AthAlgorithm(name, pSvcLocator),
  m_electron_augmenter(nullptr),
  m_electron_select(nullptr)
{
  // declareProperty("configFileName", m_config_file_name);
}

SingleBTagElectronAlg::~SingleBTagElectronAlg() {
}

// these are the functions inherited from Algorithm
StatusCode SingleBTagElectronAlg::initialize () {
  m_electron_select.reset(new AsgElectronSelectorTool("ElectronDNNSelectorLoose"));
  std::string btagging_link = "btaggingLink"; 
  // m_electron_augmenter.reset(new BTagElectronAugmenter((*m_config).btagging_link));
  m_electron_augmenter.reset(new BTagElectronAugmenter(btagging_link));
  check_rc( (*m_electron_select).setProperty("WorkingPoint", "LooseDNNElectron")); 
  check_rc( (*m_electron_select).initialize()); 

  return StatusCode::SUCCESS;
}

StatusCode SingleBTagElectronAlg::execute () {

  // dereference a few things for convenience
  auto& event = *evtStore();
  auto& electron_select = *m_electron_select;
  auto& electron_augmenter = *m_electron_augmenter;

  // most of the real work happens in this function
  const EventContext &ctx = Gaudi::Hive::currentContext();
  const xAOD::ElectronContainer *electrons = nullptr;
  check_rc( event.retrieve(electrons, "Electrons") );

  // add primary vertex
  const xAOD::VertexContainer *Primary_vertices = nullptr;
  const xAOD::Vertex *myVertex = nullptr; 
  std::string vertex_collection = "PrimaryVertices";
  // check_rc( event.retrieve(Primary_vertices, jobcfg.vertex_collection));
  check_rc( event.retrieve(Primary_vertices, vertex_collection));
  for ( const auto& vx : *Primary_vertices ) {
    if (vx->vertexType() == xAOD::VxType::PriVtx) {
      myVertex = vx;
    }
  }

  const xAOD::JetContainer *raw_jets = nullptr;
  std::string jet_collection = "AntiKt4EMPFlowJets";
  // check_rc( event.retrieve(raw_jets, jobcfg.jet_collection) );
  check_rc( event.retrieve(raw_jets, jet_collection) );
  for (const xAOD::Jet* uncalib_jet: *raw_jets) {
    // if (jobcfg.decorate.hfe) {
    //   electron_augmenter.augment(*uncalib_jet, *electrons, *myVertex, ctx, electron_select);
    // }
    electron_augmenter.augment(*uncalib_jet, *electrons, *myVertex, ctx, electron_select);
  }

  return StatusCode::SUCCESS;
}

StatusCode SingleBTagElectronAlg::finalize () {
  return StatusCode::SUCCESS;
}
