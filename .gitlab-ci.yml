variables:
  BUILD_IMAGE: gitlab-registry.cern.ch/atlas/athena/analysisbase:24.2.31
  ATHANALYSIS_IMAGE: gitlab-registry.cern.ch/atlas/athena/athanalysis:24.2.31
  ATHENA_IMAGE: registry.cern.ch/docker.io/atlas/centos7-atlasos-dev:latest-gcc11

# set the default to the AnalysisBase image
image: $BUILD_IMAGE

## Various steps to be followed by the build
stages:
  - build
  - run
  - cleanup
  - docker
  - docs
  - pages

## Common setup for all jobs
before_script:
  - set +e
  - source ~/release_setup.sh
  - if [[ -d build ]]; then source build/**/setup.sh; fi

## Compile with AnalysisBase
compile_analysisbase:
  stage: build
  script:
    - mkdir -p build && cd build
    - cmake .. | tee cmake.log
    - make clean # make sure we don't have residual compilation results
    - make -j $(nproc) 2>&1 | tee -a cmake.log # dump the log files
  artifacts:
    paths:
      - build

## Compile with AthAnalysis
compile_athanalysis:
  extends: compile_analysisbase
  image: $ATHANALYSIS_IMAGE

## Compile with Athena
compile_athena:
  tags: [k8s-cvmfs]
  before_script:
    - set +e
    - source setup/athena.sh
    - if [[ -d build ]]; then source build/**/setup.sh; fi
  extends: compile_analysisbase
  image: $ATHENA_IMAGE

###################################
## run tests
###################################

# AnalysisBase
test_pflow:
  stage: run
  needs: [compile_analysisbase]
  script:
    - test-dumper -d pflow_reduced pflow
  artifacts:
    paths:
      - pflow_reduced
    expire_in: 1 hour

test_pflow_full:
  stage: run
  needs: [compile_analysisbase]
  script:
    - test-dumper -p -d pflow_full pflow
  artifacts:
    paths:
      - pflow_full
    expire_in: 1 hour

test_slim:
  stage: run
  needs: [compile_analysisbase]
  script:
    - test-dumper slim

test_truth:
  stage: run
  needs: [compile_analysisbase]
  script:
    - test-dumper -d $PWD/ci-pflow-truth truth
  artifacts:
    paths:
      - ci-pflow-truth
    expire_in: 1 hour

test_truthjets:
  stage: run
  needs: [compile_analysisbase]
  script:
    - test-dumper truthjets

test_trackjets:
  stage: run
  needs: [compile_analysisbase]
  script:
    - test-dumper -d $PWD/ci-trackjets trackjets
  artifacts:
    paths:
      - ci-trackjets
    expire_in: 1 hour

test_trackless:
  stage: run
  needs: [compile_analysisbase]
  script:
    - test-dumper trackless

test_data:
  stage: run
  needs: [compile_analysisbase]
  script:
    - test-dumper data

# AthAnalysis
.run_athanalysis: &run-athanalysis
  image: $ATHANALYSIS_IMAGE
  stage: run
  needs: [compile_athanalysis]

test_ca:
  <<: *run-athanalysis
  script:
    - test-dumper ca

test_ca_minimal:
  <<: *run-athanalysis
  script:
    - test-dumper minimal

test_reduced_ca:
  <<: *run-athanalysis
  script:
    - test-dumper -r ca

test_multi_ca:
  <<: *run-athanalysis
  script:
    - test-dumper multi

test_ca_flow:
  <<: *run-athanalysis
  script:
    - test-dumper flow

test_ca_trigger_emtopo:
  <<: *run-athanalysis
  script:
    - test-dumper -d trig_reduced trigger-emtopo
  artifacts:
    paths:
      - trig_reduced
    expire_in: 1 hour

test_ca_trigger_emtopo_full:
  <<: *run-athanalysis
  script:
    - test-dumper -p -d trig_full trigger-emtopo
  artifacts:
    paths:
      - trig_full
    expire_in: 1 hour

test_ca_trigger_hits:
  <<: *run-athanalysis
  script:
    - test-dumper trigger-hits

test_ca_softe:
  <<: *run-athanalysis
  script:
    - test-dumper softe

test_ca_fatjet:
  <<: *run-athanalysis
  script:
    - test-dumper fatjets

test_ca_jer:
  <<: *run-athanalysis
  script:
    - test-dumper jer

# Athena
.run_athena: &run-athena
  image: $ATHENA_IMAGE
  tags: [k8s-cvmfs]
  stage: run
  needs: [compile_athena]
  before_script:
    - set +e
    - source setup/athena.sh
    - if [[ -d build ]]; then source build/**/setup.sh; fi

test_ca_athena:
  <<: *run-athena
  script:
    - test-dumper -pa athena

test_ca_retag:
  <<: *run-athena
  script:
    - test-dumper retag

test_ca_lrt:
  <<: *run-athena
  script:
    - test-dumper lrt

test_ca_trigger:
  <<: *run-athena
  script:
    - test-dumper trigger

test_ca_trigger_all:
  <<: *run-athena
  script:
    - test-dumper trigger-all

test_ca_trigger_mc21:
  <<: *run-athena
  script:
    - test-dumper trigger-mc21

test_ca_trigger_SampleA:
  <<: *run-athena
  script:
    - test-dumper trigger-SampleA

test_upgrade:
  <<: *run-athena
  script:
    - test-dumper upgrade

test_ca_trigger_trackjet:
  <<: *run-athena
  script:
    - test-dumper trigger-trackjet

test_ca_trigger_fatjet:
  <<: *run-athena
  script:
    - test-dumper trigger-fatjet

test_ca_augmented:
  <<: *run-athanalysis
  script:
    - test-dumper folds

test_ca_neutrals:
  <<: *run-athanalysis
  script:
    - test-dumper neutral

# various unit tests
test_singlebtag_submit:
  stage: run
  tags: [k8s-cvmfs]
  needs: [compile_analysisbase]
  script:
    - export USER=ci-test
    - source BTagTrainingPreprocessing/grid/setup.sh dry-run
    - grid-submit -d -f single-btag

test_configs:
  stage: run
  needs: [compile_analysisbase]
  script:
    - test-configs-single-b -v

###################################
## compare outputs
###################################

compare_precision_pflow:
  stage: run
  needs: [test_pflow_full, test_pflow]
  script:
    - h5diff -v -d 0.0003  pflow_full/output.h5 pflow_reduced/output.h5 /jets/
    - h5diff -v -p 0.001  pflow_full/output.h5 pflow_reduced/output.h5 /tracks/

compare_precision_trigger:
  stage: run
  needs: [test_ca_trigger_emtopo_full, test_ca_trigger_emtopo]
  script: h5diff -p 0.0005 trig_full/output.h5 trig_reduced/output.h5

###################################
## build images
###################################
build_img_latest: &docker_build
  needs:
    - compile_analysisbase
  stage: docker
  allow_failure: true
  image: 
    # The kaniko debug image is recommended because it has a shell, and a shell is required for an image to be used with GitLab CI/CD.
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  variables:
    IMAGE_DESTINATION: $CI_REGISTRY_IMAGE:latest
  before_script: ""
  script:
    # Set the Dockerfile FROM line to the build image.
    - DOCKER_FROM="FROM ${BUILD_IMAGE}"; sed -i "1s~.*~$DOCKER_FROM~" $CI_PROJECT_DIR/Dockerfile
    # Build and push the image from the Dockerfile at the root of the project.
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile --destination $IMAGE_DESTINATION
    # Print the full registry path of the pushed image
    - echo "Image pushed successfully to ${IMAGE_DESTINATION}"
  rules: # don't worry about this in MRs to speed things up
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PROJECT_PATH=="atlas-flavor-tagging-tools/algorithms/training-dataset-dumper"
      when: always

build_img_tag:
  <<: *docker_build
  variables:
    IMAGE_DESTINATION: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  only:
    - tags
  rules: null

###################################
## build documentation
###################################
pages:
  tags: [k8s-cvmfs]
  image: python:3.11
  stage: pages
  before_script: "" # overwrite default, do nothing
  script:
    - docs/scripts/ci-dump-variable-docs pflow ci-pflow-truth/output.h5
    - docs/scripts/ci-dump-variable-docs trackjets ci-trackjets/output.h5
    - docs/scripts/ci-mkdocs
  artifacts:
    paths:
      - public
    expire_in: 1 hour
